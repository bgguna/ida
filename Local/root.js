/* ROOT
 * Title: Implementing Distributed Algorithms over Node.js
 * Author: Bogdan Guna
 * Date: 08.12.2013
 * RUN: node root.js (<network_topology>)
 * Version: Local -- different port for each entity
 */



/* ------------------------------  Variables ------------------------------- */
var fs = require('fs');           /* filse system - read files */
var net = require('net');         /* TCP connection of root */
var host = '127.0.0.1';           /* root IP address */
var port = 8888;                  /* root port number */
var address = host + ':' + port;  /* root IP address + port number */
var entities = [];                /* store entities' addresses */
var IDs = [];                     /* store entities' IDs */
var input = process.stdin;        /* input */
var output = process.stdout;      /* output */
var protocol = [];                /* protocol - array of JSON objects */
var counter = 0;                  /* counter */
var packet = {'type':'', 'content':''}; /* JSON format of a messsage */
var neighbourhood = [];           /* store port numbers of neighbours */
var readline = require('readline'); /* reading streams line-by-line */
var user = readline.createInterface({input: input, output: output});
var prompt = 'root > ';           /* user command prompt */
var states = [];                  /* initialized states */
user.setPrompt(prompt, prompt.length); /* setting the user command prompt */
/* ------------------------------------------------------------------------- */



/* ---------------------------- Root Connection ---------------------------- */
// Create the root
var root = net.createServer({allowHalfOpen: true}, function (entity) {

  // Giving responses to entities' requests (JSON packets)
  entity.on('data', function (packet) {
    packet = packet.toString().trim();
    packet = JSON.parse(packet);
    
    // Handle entity request to register UDP port and ID
    if (packet.type === 'register') {
      entity.name = entity.remoteAddress + ':' + packet.content + ':' + packet.from;
      entity.port = packet.content;
      entity.id = packet.from;
      entity.ip = entity.remoteAddress;
      entity.status = 'online';
      entity.state = 'none';
      var idx = 0;
      if (IDs.indexOf(packet.from) >= 0) {
        entities.forEach(function (node) {
          if (node.id === entity.id) {
            entity.index = idx;
            entity.status = 'online';
            node = entity;
          }
          idx++;
        });
      } else {
        entities.push(entity);
        entity.index = entities.indexOf(entity);
        IDs.push(packet.from);
      }
      entities[entity.index].status = entity.status;
      // Send a broadcast message notifying that an entity has connected
      broadcast(entity, entity);
      output.write('\n');
      packet.type = 'neighbours';
      neighbourhood.forEach(function (node) {
        if (node.port === entity.port)
          packet.content = node.neighbours;
      });
      entity.write(JSON.stringify(packet));
      // Send neighbours details to all entities
      neighbourhood.forEach(function (node) {
        for (i = 0; i < node.neighbours.length; i++) {
          if (entity.port === node.neighbours[i].port) {
            node.neighbours[i].ip = entity.ip;
            node.neighbours[i].id = entity.id;
            node.neighbours[i].status = entity.status;
          }
        }
      });
    }

    // Acknowledgement for loading a protocol received from all entities
    if (packet.type === 'ack_loaded') {
      counter++;
      if (counter === entities.length) {
        readline.moveCursor(output, -prompt.length, -1);
        readline.clearLine(output, 1);
        output.write('\n');
        output.write('Acknowledgement received from all entities \n');
        output.write('You can safely send "run" command to all entities \n');
        output.write('\n');
        // Reset counter
        counter = 0;
      }
    }

    // Receiving states from all entities
    if (packet.type === 'change_state') {
      for (i = 0; i < entities.length; i++)
        if (entities[i].id === packet.from && entities[i].status === 'online')
          entities[i].state = packet.content;
    }

    // Handling warnings from entites
    if (packet.type === 'warning') {
      counter++;
      if (counter === 2) {
        output.write('\n');
        output.write('Error occured! \n');
        output.write('Reason: ' + packet.content + '\n');
        output.write('Please "get" neighbourhoods for the entities \n\n');
        // Reset counter
        counter = 0;
      }
    }
    
    // User command prompt
    user.emit('SIGCONT');

  });


  // TO DO on disconnecting entities.
  entity.on('end', function () {
    // Update neighbourhood entities' status and send a broadcast message
    // notifying that an entity has gone offline
    entity.status = 'offline';
    if (entity.id != 'wrong_id') {
      broadcast(entity, entity);
      entity.status = 'offline';
      output.write('\n');
      // User command prompt
      user.emit('SIGCONT');
    }
    neighbourhood.forEach(function (node) {
      for (i = 0; i < node.neighbours.length; i++)
        if (entity.id === node.neighbours[i].id)
          node.neighbours[i].status = 'offline';
    });
  });

});
/* ------------------------------------------------------------------------- */



/* -------------------------- Event Handlers ------------------------------- */
// Start the root
root.listen(port, host);


// Another root is running on the request port -- wait and retry
root.on('error', function (error) {
  if (error.code === 'EADDRINUSE') {
    output.write('Address in use. Please try again...\n');
    process.exit();
  }
});


// Listening to the root
root.on('listening', function () {
  output.write('\n\n---------- Root ----------\n\n');
  output.write('\n');
  output.write('Listening to ' + address + '\n');
  output.write('Date: ' + (new Date).toString() + '\n');
  output.write('\n');
  output.write('Enter "help" to display commands panel\n\n');


  // Provide arguments to load a network of neighbours for the entities
  if (process.argv.length === 3) {
    fs.readFile('networks/' + process.argv[2].trim() + '.txt', 'utf8', function (error, data) {

      if (error) {
        return output.write('Something went wrong\nPlease try again...\n\n');
        user.emit('SIGCONT');
      }
      

      // Store neighbourhoods in an object to manipulate data easier
      neighbourhood = eval(data);

      output.write('Loaded neighbourhoods for all entities \n');
      output.write('Entities can now connect to root \n');
      output.write('\n');
      user.emit('SIGCONT');
    });
  } else {
    output.write('Please provide neighbours for all entities \n');
    output.write('\n');
    user.emit('SIGCONT');
  }


  // Command panel
  user.on('line', function (data) {
    data = data.toString().trim();
    switch(data) {
      case 'help':
        output.write('\n');
        output.write('------------------ Commands panel --------------------------------------- \n');
        output.write('(1)  help                         -> displays commands panel              \n');
        output.write('(2)  entities                     -> displays a list of all entities      \n');
        output.write('(3)  initialize states <states>   -> set the state of all entities        \n');
        output.write('(4)  reset states                 -> reset states of all entities         \n');
        output.write('(5)  network                      -> displays the topology of the network \n');
        output.write('(6)  check protocol               -> check whether a protocol is loaded   \n');
        output.write('(7)  display protocol             -> displays rules of loaded protocol    \n');
        output.write('(8)  set topology <network>       -> load topolofy for the network        \n');
        output.write('(9)  load <protocol>              -> loads a protocol into root           \n');
        output.write('(10) disseminate protocol         -> sends the protocol to entities       \n');
        output.write('(11) run protocol (<states>)      -> order entities to run protocol       \n');
        output.write('(12) reset protocol               -> clear protocol globally              \n');
        output.write('(13) reset sender                 -> reset sender of each entity          \n');
        output.write('(14) close                        -> close root connection                \n');
        output.write('------------------------------------------------------------------------- \n');
        output.write('\n');
        break;
      case 'entities':
        output.write('\n');
        if (entities.length === 0) {
          output.write('No entity has connected yet \n');
        } else {
          output.write('List of entities (' + entities.length + '):\n');
          for (i = 0; i < entities.length; i++)
            output.write('(' + (i + 1) + ') ' + entities[i].id + ' -- ' + entities[i].status + ' -> ' + entities[i].state + '\n');
        }
        output.write('\n');
        break;
      case 'network':
        output.write('\n');
        output.write('Neighbours of each online entity: \n');
        for (i = 0; i < entities.length; i++) {
          if (entities[i].status === 'online') {
            output.write(entities[i].id + ': ');
            if (neighbourhood.length === 0)
              output.write('no neighbours yet');
            output.write('\n');
            neighbourhood.forEach(function (node) {
              if (entities[i].port === node.port) {
                for (j = 0; j < node.neighbours.length; j++) {
                  if (node.neighbours[j].status === 'offline') {
                    output.write('   (' + (j + 1) + ') ' + node.neighbours[j].port + ' -- ' + node.neighbours[j].status + '\n');
                  } else {
                    output.write('   (' + (j + 1) + ') ' + node.neighbours[j].id + ' -- ' + node.neighbours[j].status + '\n');
                  }
                }
              }
            });
          }
        }
        output.write('\n');
        break;
      case 'close':
        process.exit(0);
        break;
      default:
        // Ignore other user input
        break;
    } // switch


    // Tokenize data received from user input
    var tokens = data.toString().split(' ');


    // Read states from a file and then initialize all entities
    if (tokens[0] === 'initialize' && tokens[1] === 'states') {
      if (tokens.length === 3)
        initializeStates(tokens[2]);
      else
        output.write('\nPlease specify file to select states from \n\n');
    }


    // Reset the states of all entities to 'none'
    if (tokens[0] === 'reset' && tokens[1] === 'states') {
      if (tokens.length === 2)
        resetStates();
      else
        output.write('\nThis command does no accept additional parameters \n\n');
    }


    // Check whether a protocol is loaded or not and display its name
    if (tokens[0] === 'check' && tokens[1] === 'protocol') {
      output.write('\n');
      if (tokens.length === 2) {
        if (protocol.length === 0) {
          output.write('No protocol is currently loaded \n');
          output.write('Try "load <protocol>" command...\n');
        } else {
          output.write('Currently loaded protocol: ' + protocol[0].name + '\n');
        }
      } else {
        output.write('This command does not accept additional parameters \n');
      }
      output.write('\n');
    }


    // Display the rules of a protocol if it is loaded
    if (tokens[0] === 'display' && tokens[1] === 'protocol') {
      output.write('\n');
      if (tokens.length === 2) {
        if (protocol.length === 0) {
          output.write('No protocol is currently loaded \n');
          output.write('Try "load <protocol>" command...\n');
        } else {
          output.write('Protocol: ' + protocol[0].name + '\n');
          output.write('-------------------- \n')
          for (i = 1; i < protocol.length; i++) {
            output.write(protocol[i].state + ' ^ ' + protocol[i].event + ' -> ');
            if (protocol[i].action.length > 0) {
              for (j = 0; j < protocol[i].action.length - 1; j++) {
                output.write(protocol[i].action[j] + ', ');
              }
              output.write(protocol[i].action[protocol[i].action.length - 1] + ';\n');
            } else {
              output.write('nil; \n');
            }
          }
        }
      } else {
        output.write('This command does not accept additional parameters \n');
      }
      output.write('\n');
    }


    // Read and set the topology of the network
    if (tokens[0] === 'set' && tokens[1] === 'topology') {
      output.write('\n');
      if (tokens.length != 3) {
        output.write('This command accepts only one additional parameter \n')
      } else {
        fs.readFile('networks/' + tokens[2].toString().trim() + '.txt', 'utf8', function (error, data) {
          readline.moveCursor(output, -prompt.length, 0);
          readline.clearLine(output, 1);

          // Display a message if an error occurs
          if (error) 
            return output.write('Something went wrong\nPlease try again...\n\n');
                
          neighbourhood = eval(data);

          output.write('Loaded neighbourhoods for all entities \n');

          var packet = {'type':'neighbours', 'content':''};
          neighbourhood.forEach(function (node) {
            for (i = 0; i < entities.length; i++) {
              for (j = 0; j < node.neighbours.length; j++) {
                if (node.neighbours[j].port === entities[i].port) {
                  node.neighbours[j].ip = entities[i].ip;
                  node.neighbours[j].id = entities[i].id;
                  node.neighbours[j].status = entities[i].status;
                }
              }
            }
          });
          for (i = 0; i < entities.length; i++) {
            neighbourhood.forEach(function (node) {
              if (node.port === entities[i].port)
                packet.content = node.neighbours;
            });
            entities[i].write(JSON.stringify(packet));
          }

          user.emit('SIGCONT');

        });
      }
      output.write('\n');
    }


    // Load a protocol from the 'algorithms' folder - JSON format
    if (tokens[0] === 'load') {
      output.write('\n');
      if (tokens.length === 2) {
        fs.readFile('algorithms/' + tokens[1].toString().trim() + '.txt', 'utf8', function (error, data) {
          readline.moveCursor(output, -prompt.length, 0);
          readline.clearLine(output, 1);

          // Display a message if an error occurs
          if (error)
            return output.write('You are not loading an available protocol\nPlease retry...\n');

          // Store a protocol in JSON format
          protocol = eval(data);

          output.write('Loaded protocol: ' + protocol[0].name + ' \n');
          output.write('You can now send the protocol to online entities \n');
          output.write('\n');

          user.emit('SIGCONT');
        });
      } else {
        output.write('This command accepts only one additional parameter \n');
      }
      output.write('\n');
    }


    // Send the loaded protocol to all entities.
    if (tokens[0] === 'disseminate' && tokens[1] === 'protocol') {
      output.write('\n');
      if (protocol != '') {
        if (entities.length === 0) {
          output.write('There are no entities to disseminate the protocol to \n');
          output.write('Please retry... \n');
        } else {
          var packet = {'type':'load_protocol', 'content':protocol};
          entities.forEach(function (entity) {
            entity.write(JSON.stringify(packet));
          });
          output.write('Protocol "' + protocol[0].name + '"" disseminated to all entities \n');
          output.write('Waiting for acknowledgement to run "' + protocol[0].name + '"" ... \n');
        }
      } else {
        output.write('Please load a protocol before dissemination \n');
      }
      output.write('\n');
    }


    // Send "run" command to all entities to execute the loaded protocol
    if (tokens[0] === 'run' && tokens[1] === 'protocol') {
      if (tokens.length === 2) {
        for (i = 0; i < entities.length; i++) {
          if (entities[i].state != 'none')
            counter++;
        }
        if (counter === entities.length) {
          runProtocol();
          setTimeout(function () {
            resetSender();
          }, 2000);
        } else {
          output.write('\n');
          output.write('Please initialize states \n');
          output.write('\n');
        }
        // Reset counter for next run
        counter = 0;
      } else if (tokens.length === 3) {
        initializeStates(tokens[2]);
        setTimeout(function () {
          readline.moveCursor(output, -prompt.length, 0);
          readline.clearLine(output, 1);
          runProtocol();
        }, 2000);
        setTimeout(function () {
          resetSender();
        }, 3000);
      } else {
        output.write('\n');
        output.write('Please provide the correct number of arguments \n');
        output.write('\n');
      }
    }


    // Reset protocol from root and entities
    if (tokens[0] === 'reset' && tokens[1] === 'protocol') {
      output.write('\n');
      if (tokens.length === 2) {
        if (protocol.length === 0) {
          output.write('No protocol has been initialized \n');
          output.write('There is nothing to reset \n');
        } else {
          resetProtocol();
          var packet = {'type':'reset_protocol'};
          entities.forEach(function (entity) {
            entity.write(JSON.stringify(packet));
          });
        }
      } else {
        output.write('This command does not accept additional parameters \n');
      }
      output.write('\n');
    }


    // Reset the sender for each of the entities
    if (tokens[0] === 'reset' && tokens[1] === 'sender') {
      output.write('\n');
      if (tokens.length === 2) {
        resetSender();
        output.write('Sender has been reset for all entities \n');
      } else {
        output.write('This command does not accept additional parameters \n');
      }
      output.write('\n');
    }

    // Provide user command prompt
    user.emit('SIGCONT');
  });

});


// TO DO on user exit command
process.on('exit', function () {
  output.write('\n');
  output.write('Root connection closed\n');
});


// Override SIGTSTP and prevent root from going to background.
user.on('SIGTSTP', function () {
  output.write('\n');
});


// Resume stream
user.on('SIGCONT', function () {
  user.prompt();
});


// TO DO on 'CTRL-C' from user
user.on('SIGINT', function() {
  user.question('Are you sure you want to exit? (y/n) ', function(answer) {
    if (answer.match(/^y(es)?$/i))
      process.exit();
    if (answer.match(/^n(o)?$/i))
      user.prompt();
  });
});
/* ------------------------------------------------------------------------- */



/* ------------------------------- Functions ------------------------------- */
// Function to send a broadcast message in JSON format
function broadcast (aboutEntity, sender) {
  // The packet to be sent is a 'notification' to all connected entities
  // It will be printed out on the standard output by each entity and root
  var message = aboutEntity.ip + ':' + aboutEntity.port + ':' + aboutEntity.id + ':' + aboutEntity.status;
  var packet = {'type':'notification', 'content':''};
  packet.content = message;
  entities.forEach(function (entity) {
    if (entity === sender) {
      return;
    }
    if (entity.status === 'online') {
      entity.write(JSON.stringify(packet));
    }
  });
  readline.moveCursor(output, -prompt.length, 0);
  readline.clearLine(output, 1);
  output.write(aboutEntity.id + ' is now ' + aboutEntity.status);
} // broadcast function


// Function to initialize all entities by reading the states from a file
function initializeStates (file) {
  output.write('\n');
  fs.readFile('states/' + file.toString().trim() + '.txt', 'utf8', function (error, data) {
    readline.moveCursor(output, -prompt.length, 0);
    readline.clearLine(output, 1);

    // Display a message if an error occurs
    if (error)
      return output.write('Something went wrong\nPlease try again...\n\n');
        
    states = eval(data);

    output.write('Initializing entities... \n\n');

    if (entities.length === 0) {
      output.write('There are no entities to initialize, please retry\n')
    } else {
      output.write('States have been set: \n');
      for (i = 0; i < entities.length; i++) {
        if (entities[i].status === 'online') {
          var packet = {'type':'initialize_state', 'content':states[i].state};
          entities[i].write(JSON.stringify(packet));
          output.write(entities[i].id + ' -> ' + states[i].state + '\n');
        }
      }
    }
    output.write('\n');

    // User command prompt
    user.emit('SIGCONT');

  });
}


// Function to reset the states of all entities
function resetStates () {
  output.write('\n');
  if (entities.length === 0) {
    output.write('There are no entities to reset their state \n');
    output.write('Please retry... \n');
  } else {
    output.write('States have been reset: \n');
    for (i = 0; i < entities.length; i++) {
      if (entities[i].status === 'online') {
        var packet = {'type':'reset_state'};
        entities[i].write(JSON.stringify(packet));
        output.write(entities[i].id + ' -> none \n');
      }
    }
  }
  output.write('\n');
}


// Function to send 'run' command protocol to all entities
function runProtocol () {
  output.write('\n');
  if (protocol != '') {
    var packet = {'type':'run_protocol', 'content':protocol[0]};
    if (entities.length === 0) {
      output.write('There are no entities to send "run" command \n');
      output.write('Please retry... \n');
    } else {
      for (i = 0; i < entities.length; i++) {
        output.write('RUN sent to ' + entities[i].id + '...\n');
        entities[i].write(JSON.stringify(packet));
      }
    }
  } else {
    output.write('Please load a protocol before execution \n\n');
  }
  output.write('\n');
}


// Function to reset protocol
function resetProtocol () {
  protocol = [];
  output.write('Protocol has been reset \n');
}


// Function to message each entity to reset its sender
function resetSender () {
  var packet = {'type':'reset_sender'};
    entities.forEach(function (entity) {
      entity.write(JSON.stringify(packet));
  });
}
/* ------------------------------------------------------------------------- */

