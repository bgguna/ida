/* ENTITY
 * Title: Implementing Distributed Algorithms over Node.js
 * Author: Bogdan Guna
 * Date: 08.12.2013
 * RUN: node entity.js <entity_port_number> <entity_ID>
 * Version: Local -- different ports for each entity
 */



/* ------------------------------  Variables ------------------------------- */
var net = require('net');                 /* entity TCP connection to root */
var Buffer = require('buffer').Buffer;    /* buffer for SEND function */
var dgram = require('dgram');             /* datagram - UDP */

var host = '127.0.0.1';                   /* root IP address */
var port = 8888;                          /* root port number */
var address = host + ':' + port;          /* root IP address + port number */
var protocol = [];                        /* protocol structure */
var sender = '';                          /* entity ID of sender */
var destinator = '';                      /* entity ID of destinator */
var stateMachine = {};                    /* state machine */
var acceptableEvents = ['init', 'receive'];     /* acceptable events */
var acceptableStates = ['INITIATOR', 'IDLE', 'DONE', 'ASLEEP', 'AWAKE',
                        'FOLLOWER', 'LEADER', 'ACTIVE'];  /*acceptable states */

var root = new net.Socket({allowHalfOpen: false});  /* root using TCP port */
var myPort = process.argv[2];             /* entity UDP port number */
var myID = process.argv[3];               /* entity ID */
var myStatus = 'online';                  /* entity status */
var entity = dgram.createSocket('udp4');  /* entity using UDP port */
entity.bind(myPort);                      /* bind the port to the UDP root */
var input = process.stdin;                /* input */
var output = process.stdout;              /* output */
var state = 'none';                       /* initial state */
var context = 'chat';                     /* context of msg: chat / protocol */
var packet = {'type':'', 'from':'', 'to':[], 'content':'', 'context':context}; /* communication */
var neighbours = [];                      /* neighbours ip, port, id, status */
var msg = '';                             /* protocol message */
var readline = require('readline');       /* reading streams line-by-line */
var user = readline.createInterface({input: input, output: output});
var prompt = myID + ' > ';                /* user command prompt */
user.setPrompt(prompt, prompt.length);    /* setting the user command prompt */
/* ------------------------------------------------------------------------- */



/* ----------------------- Root Connection and Input ----------------------- */
// Connecting the root (TCP) to the Server
root = net.connect(port, host, function () {

  // Welcoming messages for each client that connects to the Server
  output.write('Connected to root - ' + address + '\n');
  output.write('Date: ' + (new Date).toString() + '\n');
  output.write('You are registered as: ' + myID + '\n\n\n')
  output.write('Enter "help" to display commands panel \n\n');


  // User command prompt
  user.emit('SIGCONT');


  // Send a JSON packet to root to register IP and UDP port
  packet.type = 'register';
  packet.from = myID;
  packet.to.push('root');
  packet.content = myPort;
  root.write(JSON.stringify(packet));


  user.on('line', function (packet) {
    // Command panel
    command = packet.toString().trim();
    switch (command) {
      case 'help':
        output.write('\n');
        output.write('------------------ Commands panel --------------------------------------- \n');
        output.write('(1) help                  -> displays commands panel                      \n');
        output.write('(2) me                    -> displays details about yourself              \n');
        output.write('(3) set state <state>     -> change current state to <state>              \n');
        output.write('(4) reset state           -> reset state ("none")                         \n');
        output.write('(4) neighbours            -> displays a list of your neighbours           \n');
        output.write('(5) send <id> <msg>       -> sends a message to a neighbour               \n');
        output.write('(6) send all <msg>        -> sends a broadcast message to your neighbours \n');
        output.write('(7) reset sender          -> reset sender for broadcasting                \n');
        output.write('(8) display protocol      -> if a protocol is loaded, display its rules   \n');
        output.write('(9) close                 -> go offline                                   \n');
        output.write('------------------------------------------------------------------------- \n');
        output.write('\n');
        break;
      case 'me':
        output.write('\n');
        output.write('Your ID: ' + myID + '\n');
        output.write('Current state: ' + state + '\n');
        output.write('Loaded protocol: ');
        if (protocol.length === 0)
          output.write('none \n');
        else
          output.write(protocol[0].name + '\n');
        output.write('Uptime: ' + process.uptime() + ' seconds \n\n');
        break;
      case 'neighbours':
        output.write('\n');
        if (neighbours === myPort) {
          output.write('Topology of network has not been specified yet \n');
        } else {
          output.write('List of your neighbours (' + neighbours.length + '): \n');
          for (i = 0; i < neighbours.length; i++) {
            if (neighbours[i].id === '')
              neighbours[i].id = 'unknown id';
            output.write('(' + (i + 1) + ') ' + neighbours[i].id + ' -- ' + neighbours[i].status + '\n');
          }
        }
        output.write('\n');
        break;
      case 'close':
        process.exit();
        break;
      default:
        // Ignore other user input
        break;
    } // switch


    // Tokenize input data to recognise commands
    var tokens = packet.toString().split(' ');


    // Change current state to an acceptable state
    if (tokens[0] === 'set' && tokens[1] === 'state') {
      output.write('\n');
      if (tokens.length === 3)
        become(tokens[2]);
      else
        output.write('This command accepts only one additional parameter \n');
      output.write('\n');
    }


    // Reset the current state to the initial current state ('none')
    if (tokens[0] === 'reset' && tokens[1] === 'state') {
      output.write('\n');
      if (tokens.length === 2)
        resetState();
      else
        output.write('This command does not accept additional parameters \n');
      output.write('\n');
    }


    // Send messages to am emtoty by specifying its ID
    if (tokens[0] === 'send' && tokens[1] != 'all') {
      if (tokens.length > 2) {
        conext = 'chat';
        var message = '';
        destinator = tokens[1];
        for (i = 2; i < tokens.length; i++)
          message += ' ' + tokens[i];
        message = message.trim();
        send(message, destinator);
      } else {
        output.write('\n');
        output.write('This command needs at least one additional parameter \n');
        output.write('\n');
      }
    }


    // Reply to sender (without specifying its ID -- for simplicity)
    if (tokens[0] === 'reply') {
      if (tokens.length > 1) {
        context = 'chat';
        var message = '';
        destinator = sender;
        for (i = 1; i < tokens.length; i++)
          message += ' ' + tokens[i];
        message = message.trim();
        send(message, sender);
      } else {
        output.write('\n');
        output.write('This command needs at least one additional parameter \n');
        output.write('\n');
      }
    }


    // Send messages to all entities (broadcast)
    if (tokens[0] === 'send' && tokens[1] === 'all') {
      if (tokens.length > 2) {
        context = 'chat';
        var message = '';
        for (i = 2; i < tokens.length; i++)
          message += ' ' + tokens[i];
        message = message.trim();
        // Send a message that is recognised by the server,
        // to retrieve the list of all clients and then broadcast a message.
        send(message, 'all');
      } else {
        output.write('\n');
        output.write('This command needs at least one additional parameter \n');
        output.write('\n');
      }
    }


    // Reset sender for sending broadcasting messages to all neighbours
    if (tokens[0] === 'reset' && tokens[1] === 'sender') {
      output.write('\n');
      if (tokens.length === 2)
        resetSender();
      else
        output.write('This command does not accept additional parameters \n');
      output.write('\n');
    }


    // If a protocol is loaded, display its rules
    if (tokens[0] === 'display' && tokens[1] === 'protocol') {
      output.write('\n');
      if (tokens.length === 2) {
        if (protocol.length === 0) {
          output.write('No protocol is currently loaded \n');
          output.write('Wait for a protocol to be sent (root) \n');
        } else {
          output.write('Protocol: ' + protocol[0].name + '\n');
          output.write('-------------------- \n')
          for (i = 1; i < protocol.length; i++) {
            output.write(protocol[i].state + ' ^ ' + protocol[i].event + ' -> ');
            if (protocol[i].action.length > 0) {
              for (j = 0; j < protocol[i].action.length - 1; j++) {
                output.write(protocol[i].action[j] + ', ');
              }
              output.write(protocol[i].action[protocol[i].action.length - 1] + ';\n');
            } else {
              output.write('nil; \n');
            }
          }
        }
      } else {
        output.write('This command does not accept additional parameters \n');
      }
      output.write('\n');
    }


    // User command prompt
    user.emit('SIGCONT');

  });

});
/* ------------------------------------------------------------------------- */



/* ----------------------------- Handling Events ------------------------------ */
// TO DO if root is offline
root.on('error', function (error) {
  if (error.code === 'ECONNREFUSED') {
    output.write('Root is offline. Please retry :-) \n');
    process.exit();
  }
});


// TO DO on user exit command
process.on('exit', function () {
  readline.moveCursor(output, -prompt.length, 0);
  readline.clearLine(output, 1);
  output.write('\n');
  output.write('You are now offline \n');
})


// Override SIGTSTP and prevent root from going to background.
user.on('SIGTSTP', function () {
  output.write('\n');
});


// Resume stream
user.on('SIGCONT', function () {
  user.prompt();
});


// TO DO on 'CTRL-C' from user
user.on('SIGINT', function() {
  user.question('Are you sure you want to go offline? (y/n) ', function(answer) {
    if (answer.match(/^y(es)?$/i))
      process.exit();
    if (answer.match(/^n(o)?$/i))
      user.prompt();
  });
});


// TO DO on closing root
root.on('end', function () {
  process.exit();
});


// TO DO on running Entity
entity.on('listening', function() {
  output.write('\n\n---------- Entity ----------\n\n');
  output.write('\n');
});


// TO DO on receiving JSON packets from other entities
// packet = {type, from, to[], content}
entity.on('message', function (packet) {
  packet = packet.toString().trim();
  packet = JSON.parse(packet);

  // Store id of sender
  sender = packet.from;

  readline.moveCursor(output, -prompt.length, 0);
  readline.clearLine(output, 1);

  if (packet.context === 'chat') {
    if (packet.type === 'broadcast')
      output.write(sender + ' (broadcast): ' + packet.content + '\n');
    if (packet.type === 'private')
      output.write(sender + ': ' + packet.content + '\n');
  }

  if (packet.context === 'protocol') {
    msg = packet.content;
  }

  // Provide user command prompt
  user.emit('SIGCONT');
  
});


// TO DO on receiving JSON packets from root
// packet = {type, from, to[], content}
root.on('data', function (packet) {
  packet = packet.toString().trim();
  packet = JSON.parse(packet);
  
  readline.moveCursor(output, -prompt.length, 0);
  readline.clearLine(output, 1);

  // Receiving port numbers of all of your neighbours - initial status: offline
  if (packet.type === 'neighbours') {
    output.write('\n');
    output.write('Received neighbours details (root) \n');
    output.write('\n');
    neighbours = packet.content;
  }

  // Packet is a command to initialize state
  if (packet.type === 'initialize_state') {
    output.write('Received state initialization (root) \n');
    become(packet.content);
  }

  // Packet is a command to reset state
  if (packet.type === 'reset_state') {
    output.write('Received task to reset state (root) \n');
    resetState();
  }

  // Packet is a notification from root -> output it
  if (packet.type === 'notification') {
    if (typeof neighbours === 'undefined') {
      root.write(JSON.stringify({'type':'warning', 'content':'no neighbours have been provided'}));
      output.write('No neighbours have been provided \n');
      output.write('Please retry... \n');
      process.exit();
    }
    var data = packet.content.toString().split(':');
    for (i = 0; i < neighbours.length; i++) {
      if (neighbours[i].port === data[1]) {
        output.write(data[2] + ' is now ' + data[3] + '\n');
        neighbours[i].ip = data[0];
        neighbours[i].id = data[2];
        neighbours[i].status = data[3];
      }
    }
  }


  // Packet contains a protocol
  // Send acknowledgement to root after receival
  if (packet.type === 'load_protocol') {
    protocol = packet.content;
    output.write('Received ' + protocol[0].name + ' protocol (root) \n');
    output.write('Loaded ' + protocol[0].name + ' protocol \n');
    root.write(JSON.stringify({'type':'ack_loaded', 'from':myID}));
    output.write('Acknowledgement has been sent to root for further instructions \n');
    output.write('\n');
  }


  // Packet contains a "run" command
  // Run the loaded protocol and send a finish message to root
  if (packet.type === 'run_protocol') {
    output.write('\nReceived "run" command to execute ' + protocol[0].name + ' protocol \n');
    output.write('Executing ' + protocol[0].name + ' protocol... \n\n');

    context = 'protocol';

    msg = 'some message';

    executeProtocol();
  }


  // Receving instruction to reset protocol
  if (packet.type === 'reset_protocol') {
    resetProtocol();
  }


  // Receiving instruction to reset sender
  if (packet.type === 'reset_sender') {
    resetSender();
  }
  

  // User command prompt
  user.emit('SIGCONT');

});
/* ------------------------------------------------------------------------- */



/* ----------------------------- State Machine ----------------------------- */
stateMachine['INITIATOR'] = {
  'init': interpret('INITIATOR', 'init'),
  'receive': interpret('INITIATOR', 'receive')
};

stateMachine['IDLE'] = {
  'init': interpret('', 'init'),
  'receive': interpret('', 'receive')
};

stateMachine['DONE'] = {
  'init': interpret('DONE', 'init'),
  'receive': interpret('DONE', 'receive')
};

stateMachine['ASLEEP'] = {
  'init': interpret('ASLEEP', 'init'),
  'receive': interpret('ASLEEP', 'receive')
};

stateMachine['AWAKE'] = {
  'init': interpret('AWAKE', 'init'),
  'receive': interpret('AWAKE', 'receive')
};

stateMachine['FOLLOWER'] = {
  'init': interpret('FOLLOWER', 'init'),
  'receive': interpret('FOLLOWER', 'receive')
};

stateMachine['LEADER'] = {
  'init': interpret('LEADER', 'init'),
  'receive': interpret('LEADER', 'receive')
};
/* ------------------------------------------------------------------------- */



/* ------------------------------- Functions ------------------------------- */
// Function to send messages from an entity to:
// (1) all neighbours,
// (2) all neighbours, excluding the sender,
// (3) a single neighbour.
function send (message, destination) {
  destination = destination.toLowerCase();
  packet.from = myID;
  packet.content = message;
  packet.context = context;

  if (destination === 'all') {
    packet.type = 'broadcast';
    packet.to = 'all';
    var buffer = new Buffer(JSON.stringify(packet));
    for (i = 0; i < neighbours.length; i++)
      if (sender != neighbours[i].id && neighbours[i].status === 'online')
        entity.send(buffer, 0, buffer.length, neighbours[i].port, neighbours[i].ip);
    output.write('Message sent as broadcast: "' + message + '"\n');
  } else {
    var foundDestinator = false;
    packet.type = 'private';
    packet.to = destination;
    var buffer = new Buffer(JSON.stringify(packet));
    for (i = 0; i < neighbours.length; i++) {
      if (destinator === neighbours[i].id && neighbours[i].status === 'online') {
        foundDestinator = true;
        entity.send(buffer, 0, buffer.length, neighbours[i].port, neighbours[i].ip);
        output.write('Message sent to ' + neighbours[i].id + ': "' + message + '"\n');
      } else if (destinator === neighbours[i].id && neighbours[i].status != 'online') {
        foundDestinator = true;
        output.write('Error: could not send message to "' + neighbours[i].id + '"\n');
        output.write('Reason: "' + neighbours[i].id + '"" is not online \n\n');
      }
    }
    if (foundDestinator === false) {
      if (destinator === '') {
        output.write('Error: please specifiy a destinator of your message \n\n');
      } else {
        output.write('Error: could not send message to "' + destinator + '"\n');
        output.write('Reason: "' + destinator + '"" is not your neighbour \n\n');
      }
    }
  }
}


// Function to print a message
function print (message) {
  output.write('Received: "' + message + '"\n');
}


// Function to change the state of an entity
function become (newState) {
  newState = newState.toUpperCase();
  if (acceptableStates.indexOf(newState) < 0) {
    output.write('Error: ' + newState + ' is not an acceptable state\n');
    return;
  }
  output.write('Changed state from ' + state + ' to ' + newState + '\n');
  root.write(JSON.stringify({'type':'change_state', 'from':myID, 'content':newState}));
  state = newState;
}


// Function to interpret: state ^ event -> action
function interpret (STATE, EVENT) {
  for (i = 1; i < protocol.length; i++)
    if (STATE === protocol[i].state && EVENT === protocol[i].event)
      protocol[i].action.forEach(function (operation) { eval(operation);});
}


// Function to reset sender
function resetSender () {
  if (sender === '') {
    output.write('There is nothing to reset \n');
  } else {
    sender = '';
    output.write('Sender has been reset \n');
  }
}


// Function to reset protocol
function resetProtocol () {
  protocol = [];
  output.write('Protocol has been reset \n');
}


// Function to reset state
function resetState () {
  if (state === 'none') {
    output.write('There is nothing to reset \n');
  } else {
    state = 'none';
    output.write('State has been reset (' + state + ') \n');
    root.write(JSON.stringify({'type':'change_state', 'from':myID, 'content':state}));
  }
}


// Function to execute protocol
function executeProtocol () {
  acceptableStates.forEach(function (stateIndex) {
    if (stateIndex === state) {
      acceptableEvents.forEach(function (eventIndex) {
        interpret(stateIndex, eventIndex);
      });
    }
  });
}
/* ------------------------------------------------------------------------- */

